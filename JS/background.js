var filibusterIsOn;
chrome.storage.sync.get("filibusterIsOn", function(result) {
    filibusterIsOn = result.filibusterIsOn;
});

chrome.browserAction.onClicked.addListener(function(tab) {
    filibusterIsOn = !filibusterIsOn;
    chrome.browserAction.setIcon({ path: filibusterIsOn ? "icon/iconOn.png" : "icon/iconOff.png"});
    chrome.storage.sync.set({'filibusterIsOn': filibusterIsOn});
    chrome.tabs.sendMessage(tab.id, filibusterIsOn);
    if(!filibusterIsOn){
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.reload(tabs[0].id);
        });
    }

});

chrome.runtime.onInstalled.addListener(function (object) {
    if(object.reason === 'install') {
        chrome.tabs.create({url: "html/installed.html"});
    }
});

