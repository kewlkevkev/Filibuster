const INTERVAL = 250;
const THRESHOLD = 3.85;

var isOn;
chrome.storage.sync.get("filibusterIsOn", function(result) {
    isOn = result.filibusterIsOn;
});


var log = [];
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
    if(xhr.readyState == 4) {
        log = xhr.responseText.split("\n");
        for(i = 0; i < log.length; i++) {
            log[i] = log[i].split(",");
            log[i][1] = parseFloat(log[i][1]);
        }
        setInterval(searchAndDestroy, INTERVAL);
    }
};
xhr.open("GET", chrome.extension.getURL('Java/weightedWords.js'), true);
xhr.send();


function searchAndDestroy() {
    if (isOn) {
        var list = document.getElementsByClassName("tweet");
        for (var i = 0; i < list.length; i++) {
            if (list[i].className.indexOf("filibuster-marked ") == -1) {
                if (identifyDiv(list[i])) {
                    removeDiv(list[i]);
                } else {
                    list[i].className = "filibuster-marked " + list[i].className;
                }
            }
        }
    }
}

function removeDiv(div) {
    div.parentElement.removeChild(div);
}

function identifyDiv(div) {
    var sum = 0;
    for(i = 0; i < log.length; i++) {
        if(div.innerHTML.toLowerCase().indexOf(log[i][0]) != -1) {
            if (isNaN(log[i][1])) {
                console.log("Invalid entry: #" + i);
            } else {
                sum += log[i][1];
                //console.log(log[i][0] + " " + sum);
            }
        }
    }
    if(sum >= THRESHOLD) {
       // console.log("removed");
        return true;
    }
    //console.log("not removed");
    return false; 
}

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        isOn = request;
    });