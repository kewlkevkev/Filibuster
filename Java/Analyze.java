import java.util.*;
import java.io.*;

public class Analyze {
    private static final int NUMRESULTS = 300;

    private static ArrayList<Node> nodes;

    public static void main(String [] args) throws IOException {
        if (args.length != 5) {
            System.out.println("Please run with: \"java Analyze <political file> <apolitical file> <common words> <exceptions> <additions>\"");
            return;
        }
        nodes = new ArrayList<Node>();
        calculate(args[0], 1);
        calculate(args[1], -1);
        Scanner s = new Scanner(new File(args[2]),"UTF-8");
        while (s.hasNext()) {
            String str = s.next().toLowerCase();
            while (nodes.remove(new Node(str, 0)));
        }
        s.close();

        s = new Scanner(new File(args[3]), "UTF-8");
        while (s.hasNext()) {
            String str = s.next().toLowerCase();
            while (nodes.remove(new Node(str,0)));
        }
        s.close();
        
        s = new Scanner(new File(args[4]), "UTF-8");
        while(s.hasNext()) {
            String[] str = s.next().toLowerCase().split(",");
            nodes.add(new Node(str[0], Double.parseDouble(str[1])));
        }
        s.close();

        Collections.sort(nodes);
        PrintWriter printer = new PrintWriter(new File("weightedWords.js"));
        printer.print(nodes.get(0).word+","+nodes.get(0).score);
        for (int i = 1; i < NUMRESULTS; i ++) {
            printer.println();
            printer.print(nodes.get(i).word+","+nodes.get(i).score);
        }
        printer.close();
    }

    private static void calculate(String file, int value) throws IOException {
        double mult = new File(file).length() / 10000.;
        Scanner s = new Scanner(new File(file),"UTF-8");
        while (s.hasNext()) {
            String str = s.next().toLowerCase().replaceAll("[^A-Za-z0-9 ]", "");
            if (nodes.contains(new Node(str, 0))) {
                nodes.get(nodes.indexOf(new Node(str, 0))).score += (double) value / mult;
            } else {
                nodes.add(new Node(str, (double) value / mult));
            }
        }
        s.close();
    }
}

class Node implements Comparable<Node>{
    String word;
    double score;
    Node() {}

    Node(String word, double score) {
        this.word = word;
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        return word.equals(((Node)o).word);
    }

    public int compareTo(Node o) {
        if (score > ((Node)o).score) return -1;
        else if (score < ((Node)o).score) return 1;
        return 0;
    }
}