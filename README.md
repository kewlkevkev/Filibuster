# Filibuster

## [Chrome Web Store](https://chrome.google.com/webstore/detail/filibuster/fnaggfhmbnamcgkakjnapfjpkfjccmha)

## About

Filibuster was initially created in under 24 hours by Clark Huey and Kevin Klute.

It won 3rd place at SteelHacks 2017.

Filibuster is a chrome extension designed to hide all political tweets from the feed.

Contributions are welcome.

## Workflow

An issue moves through 5 major states (which have labels and lists on the board).

1. Icebox - The issue is ready to be worked on but hasn't been started.
2. Design - The issue is being design and/or discussed. This can be skipped.
3. Development - The issue is currently assigned and being worked on. 
When an issue enters Development a branch should be created for it. 
When an issue exits Development a Merge Request should be created.
4. CR - Someone other than the developer who worked on the issue is both reading and testing the changes to approve them. 
When an issue exits CR the merge request should be approved.
5. Done - The issue has been finished and is closed.

Issues can be organized into milestones representing larger goals or releases.

Releases should be tagged.

## Building codetable

-- under construction --

## Future goals

-- under construction --
