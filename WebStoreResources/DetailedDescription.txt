Filibuster is a chrome extension that attempts to remove all political tweets from the feed.
It was originally created in under 24 hours at SteelHacks 2017 by Clark Huey and Kevin Klute, taking 3rd place.
All of our code is open source and available at https://gitlab.com/kewlkevkev/Filibuster/ .
Contributions are welcome!

Filibuster is not designed to help you shut yourself in a bubble away from facts and dissenting opinions.
Politics is more important now than ever. Stay informed. Stay open-minded. Keep voting.
When you need a break from that to look at a few memes, click that little button in the corner.

